const initialState = {
    burgerDetails: [],
    copyofBurgerDetails: [],
    isLoading: true,
    isLoadingItemsList: true,  
    buttonsToSort: [{
        id: 1,
        name: 'Created',
        selected: true
      },
      {
        id: 2,
        name: 'Name',
        selected: false
      },
      {
        id: 3,
        name: 'Most Popular',
        selected: false
      },
      {
        id: 4,
        name: 'Least Popular',
        selected: false
      },
      {
        id: 5,
        name: 'Approved',
        selected: false
    }]
}


const burgerReducer = (state=initialState, action) => {
    switch(action.type){
        case 'IS_ITEMS_LIST_LOADING':
            return{
                ...state,
                isLoadingItemsList:true
            }
        case 'IS_PAGE_LOADING':
            return{
                ...state,
                isLoading:true,
            }
        case 'FETCH_BURGERS_LIST':
            return{
                ...state, 
                burgerDetails: action.payload,
                isLoading: false,
                isLoadingItemsList: false,
                copyofBurgerDetails: action.payload,
            }
        case 'UP_BUTTON_CLICKED':
            const tempStateUpButton = {...state}
            const changedState = tempStateUpButton.burgerDetails.map((eachButton)=>
                    action.id === eachButton.id ? {...eachButton, votes: eachButton.votes+1} : {...eachButton, votes: eachButton.votes}
            )
            return{
                ...state,
                burgerDetails: [...changedState]
            }
        case 'DOWN_BUTTON_CLICKED':
            const tempStateDownButton = {...state};
            const changedStateDownButton = tempStateDownButton.burgerDetails.map((eachButton)=>
                    action.id === eachButton.id ? {...eachButton, votes: eachButton.votes <= 0 ? '0' : eachButton.votes-1} : {...eachButton, votes: eachButton.votes}
            )
            return{
                ...state,
                burgerDetails: [...changedStateDownButton]
            }
        case 'CREATE_NEW_BURGER':
            return{
                ...state,
                burgerDetails: [...state.burgerDetails, action.payload],
                copyofBurgerDetails: [...state.burgerDetails, action.payload],
            }
        case 'APPROVED':
            const burgerItem = state.burgerDetails.find((element) => {
                return element.id === action.id;
              })
            const filteredItems = state.burgerDetails.filter(p => p.id !== action.id);
            filteredItems.splice(action.id-1, 0, {...burgerItem, approved: !burgerItem.approved});
            return{ 
                ...state, 
                burgerDetails: [...filteredItems],
                copyofBurgerDetails: [...filteredItems]
            }
        case 'SORT_BUTTON_SET_TRUE':
            const tempState1 = {...state};
            const clickedButton1 = tempState1.buttonsToSort.map((eachButton)=>
                    action.id === eachButton.id ? {...eachButton, selected: true} : {...eachButton, selected: false}
            )
            return{
                ...state,
                buttonsToSort: clickedButton1,
            }
        case 'SORT_BUTTON_CLICKED_REDUCER':
            const tempState = {...state};

            const clickedButton = tempState.buttonsToSort.map((eachButton)=>
                    action.id === eachButton.id ? {...eachButton, selected: true} : {...eachButton, selected: false}
            )
            const name = clickedButton.find((element)=> element.selected).name; //Most POPULAR
            let sortedDetails = [...tempState.copyofBurgerDetails];
            if(name === 'Most Popular')
            {
                sortedDetails = [...tempState.copyofBurgerDetails.sort((a,b) => b.votes - a.votes)];
            } else 
            if(name === 'Least Popular')
            {
                sortedDetails = [...tempState.copyofBurgerDetails.sort((a,b)=> a.votes - b.votes)]
            } else 
            if(name === 'Created'){
                sortedDetails = [...tempState.copyofBurgerDetails.sort((a,b)=> a.created - b.created)]
            }
            if(name === 'Name'){
                sortedDetails = [...tempState.copyofBurgerDetails.sort((a,b)=> a.name < b.name)]
            }
            if(name === 'Approved'){
                sortedDetails = [...tempState.copyofBurgerDetails.filter((element)=> element.approved === true )]
            }
            return{
                ...state,
                burgerDetails: sortedDetails,
                isLoading: false,
                isLoadingItemsList: false,
                buttonsToSort: clickedButton,
            }
        case 'SEARCH_INPUT_ENTERED':
            const newArray = [...state.copyofBurgerDetails];
            const filteredBySearch = newArray.filter(eachItem=>eachItem.name.includes(action.value))
            return{
                ...state,
                burgerDetails: filteredBySearch,
            }
        default:
            return state
    }
}

export default burgerReducer;