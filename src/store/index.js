import { combineReducers } from 'redux';
import burgerReducer from './burgerReducer';

const allReducers = combineReducers({

    burgers: burgerReducer,


})

export default allReducers;