
export const fetchBurgers = (data) => {
    return({
        type: 'FETCH_BURGERS_LIST',
        payload: data,
    })
}

export const pageLoading = () => {
    return({
        type: 'IS_PAGE_LOADING'
    })
}

export const itemListLoading = () => {
    return({
        type: 'IS_ITEMS_LIST_LOADING'
    })
}

export const createNewBurger = (data) =>{
    return({
        type: 'CREATE_NEW_BURGER',
        payload: data
    })
}   

export const onApproved = (id) => {
    return{
        type: 'APPROVED',
        id: id
    }
}

export const buttonsToSortClick = (btnDetails) => {
    return { 
        id:btnDetails.id,
        type: 'SORT_BUTTON_CLICKED'
     }
};

export const onUpButton = (btnDetails) => {
    return { 
        id:btnDetails.id,
        type: 'UP_BUTTON_CLICKED'
     }
};

export const onDownButton = (btnDetails) => {
    return { 
        id:btnDetails.id,
        type: 'DOWN_BUTTON_CLICKED'
     }
};

export const searchInput = (inputValue) => {
    return{
        value: inputValue,
        type: 'SEARCH_INPUT'
    }
}
  
