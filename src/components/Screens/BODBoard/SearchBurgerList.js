import React from 'react';
import { useDispatch } from 'react-redux'
import styled from 'styled-components';
import { onApproved, onUpButton, onDownButton, searchInput } from '../../../actions/burgerBoardActions';


const Span = styled.span`
color: ${props => props.approved ? 'green' :''}
`;


const SearchBurgerList = (props) => {

    const dispatch = useDispatch();

    const searchInputHandler = (event) => {
        event.preventDefault();
        dispatch(searchInput(event.target.value));
    }

    return(
        <div>
        
            <div className="input-group mb-3 margin-top50">
                <div className="input-group-prepend">
                    <button className="btn btn-outline-secondary" type="button" id="button-addon1">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-search" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"/>
                            <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"/>
                        </svg>
                    </button>
                </div>
            <input type="text"
             className="form-control"
             placeholder="search a burger"
             aria-label="Example text with button addon"
             aria-describedby="button-addon1"
             name="searchInput"
             onChange={searchInputHandler}
            />
            </div>
            {props.isLoadingItemsList && '...Loading'}
            {!props.isLoadingItemsList && props.burgersList.map((burgerItem)=>{
                return(
                    <div className="card margin-top10" key={burgerItem.id}>
                        <button className="card-container" onDoubleClick={()=>dispatch(onApproved(burgerItem.id))}>
                            <div className="counter  margin10">
                                <Span approved={burgerItem.approved}>
                                    {burgerItem.votes}
                                </Span>
                            </div>
                            <div className="card-body">
                                <Span approved={burgerItem.approved}>
                                    {burgerItem.name}
                                </Span>
                            </div>
                            <div className="vote">
                                <div>
                                    <button type="button" className="btn" onClick={()=>dispatch(onUpButton(burgerItem))}>
                                        <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-caret-up" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M3.204 11L8 5.519 12.796 11H3.204zm-.753-.659l4.796-5.48a1 1 0 0 1 1.506 0l4.796 5.48c.566.647.106 1.659-.753 1.659H3.204a1 1 0 0 1-.753-1.659z"/>
                                        </svg>
                                    </button>
                                </div>
                                <div>
                                    <button type="button" className="btn" onClick={()=>dispatch(onDownButton(burgerItem))}>
                                        <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-caret-down" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M3.204 5L8 10.481 12.796 5H3.204zm-.753.659l4.796 5.48a1 1 0 0 0 1.506 0l4.796-5.48c.566-.647.106-1.659-.753-1.659H3.204a1 1 0 0 0-.753 1.659z"/>
                                        </svg>
                                    </button>
                                </div>
                            </div>
                        </button>
                    </div>
                )
            })}
            
        </div>
    )

}

export default SearchBurgerList;