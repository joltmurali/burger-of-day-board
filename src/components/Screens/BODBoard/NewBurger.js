import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { createNewBurger } from './../../../actions/burgerBoardActions'


const NewBurger = (props) => {

    const dispatch = useDispatch();

    const [newBurger, setNewBurger] = useState('');

    const handleInputChange = (event) =>{
        event.preventDefault();
        setNewBurger(event.target.value);
    }

    const handleSave = () => {
        if(newBurger.length <= 0){
            alert('Please Enter Burger Name')
        }else{
            const tempBurgerObj = { 
                id: props.burgerDetails.length+1,
                name: newBurger,
                votes: 0,
                approved: false,
                created: new Date(),
                updated: 1596254400000
            }
            dispatch(createNewBurger(tempBurgerObj));
        }
    }

    return(
        <div>
          <header>
            <h1>New Burger Idea</h1>
          </header>
          <div className="input-group mb-3"> 
            <input
                type="text" 
                className="form-control"
                placeholder="Burger Name" 
                aria-label="Recipient's username" 
                aria-describedby="button-addon2" 
                onChange={handleInputChange}
            />
            <div className="input-group-append">
                <button 
                    className="btn btn-outline-secondary" 
                    type="button" 
                    id="button-addon2"
                    onClick={handleSave}
                >
                    Save
                </button>
            </div>
          </div>
        </div>
    )

}

export default NewBurger;