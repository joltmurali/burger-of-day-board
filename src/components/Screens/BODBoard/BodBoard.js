import React, { useState, useEffect } from 'react';
import axios from 'axios';
import NewBurger from './NewBurger';
import SortBurgerList from './SortBurgerList';
import SearchBurgerList from './SearchBurgerList';
import BodPaginator from './BodPaginator';
import { useDispatch, useSelector } from 'react-redux';
import { fetchBurgers, pageLoading } from '../../../actions/burgerBoardActions';


const BodBoard = () => {

    
    const isLoading = useSelector(state=>state.burgers.isLoading);
    const burgerDetails = useSelector(state=>state.burgers.burgerDetails);
    const isLoadingItemsList = useSelector(state=>state.burgers.isLoadingItemsList);

    const dispatch = useDispatch();


    const [currentPage, setCurrentPage] = useState(1);
    const [itemPerPage, setItemsPerPage] = useState(4);
    const [paginatedBODList, setPaginatedBODList] = useState([]);
    
    

    useEffect(()=>{
        const fetchBurgetList = () => {
            axios.get(`http://localhost:3001/burgers`)
                .then(res => {
                    dispatch(pageLoading());
                    const response = res.data;
                    dispatch(fetchBurgers(response));
                })
        }
        fetchBurgetList();
    }, [])

   useEffect(()=>{
    if(burgerDetails && burgerDetails.length>=0){
        const paginatedListOfBurgers = burgerDetails.slice((currentPage-1)*itemPerPage, itemPerPage*currentPage);
        setPaginatedBODList(paginatedListOfBurgers);
    }
   },[burgerDetails, currentPage, itemPerPage])

    const pagiatedListCallback = (pageNumber) => {
        setCurrentPage(pageNumber)
    }
        
    return(
        <div>
        {isLoading && '...Loading'}
        {!isLoading && burgerDetails &&
            <div>
            <NewBurger burgerDetails={burgerDetails}/>
            <SortBurgerList />
            <SearchBurgerList 
                burgersList={paginatedBODList && paginatedBODList}
                isLoadingItemsList={isLoadingItemsList}
            />
            <BodPaginator
            totalItemCount={burgerDetails.length}
            pagiatedListCallback={pagiatedListCallback}
            currentPage={currentPage}
            itemPerPage={itemPerPage}
            isLoadingItemsList={isLoadingItemsList}
            />
            </div>
        }
        </div>
    )

}

export default BodBoard;