import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { buttonsToSortClick } from '../../../actions/burgerBoardActions';


const SortBurgerList = () => {

    const buttonsToSort = useSelector(state=>state.burgers.buttonsToSort);

    const dispatch = useDispatch();
    return(
        <div>
        <hr className="col-xs-12" />
          <header>
            <h1>Burgers List</h1>
          </header>
            {buttonsToSort.map((btn)=>
              <button className={btn.selected ? 'btn margin10 btn-info' : 'btn margin10 btn-light'} onClick={()=>dispatch(buttonsToSortClick(btn))}>{btn.name}</button> 
            )}
        </div>
    )

}

export default SortBurgerList;