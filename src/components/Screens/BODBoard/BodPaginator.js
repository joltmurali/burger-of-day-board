import React from 'react';
import Pagination from "react-js-pagination";


const BodPaginator = (props) => {

  return(
    <div>
    {!props.isLoadingItemsList &&
        <div className='margin10 paginator'>
            <Pagination
              activePage={props.currentPage}
              itemsCountPerPage={props.itemPerPage}
              totalItemsCount={props.totalItemCount}
              pageRangeDisplayed={5}
              onChange={props.pagiatedListCallback}
            />
          </div>
    }
    </div>
  )
}

export default BodPaginator;