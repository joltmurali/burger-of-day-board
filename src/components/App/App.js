import React from 'react';
import'../Styles/app.css';
import BodBoard from '../Screens/BODBoard/BodBoard';

function App() {
  return (
    <div className="Container text-monospace">
        <BodBoard />
    </div>
  );
}

export default App;
