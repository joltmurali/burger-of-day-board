import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App/App';
import  { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import "regenerator-runtime/runtime";
import { composeWithDevTools } from 'redux-devtools-extension';
import rootSaga from './sagas';
import allReducers from './store';

const sagaMiddleware = createSagaMiddleware();

const myStore = createStore(
  allReducers,
  composeWithDevTools(
  applyMiddleware(sagaMiddleware),
  ),
);

ReactDOM.render(
  <Provider store={myStore}>
    <React.StrictMode>
      <App />
    </React.StrictMode>
  </Provider>,
  document.getElementById('root')
);

sagaMiddleware.run(rootSaga);