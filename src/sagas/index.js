import { watchButtonClick } from '../sagas/sortButtons';
import { searchInputWatcher } from '../sagas/searchInput';
import { all } from 'redux-saga/effects';

export default function* rootSaga() {
  yield all([
    watchButtonClick(),
    searchInputWatcher()
  ])
}