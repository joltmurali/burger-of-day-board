
import { takeEvery, put, delay } from 'redux-saga/effects';

export function* buttonsToSortClickAsync(btnDetails) {
    yield put({ 
        type: 'SORT_BUTTON_SET_TRUE',
        id: btnDetails.id
     })
    yield put({ 
        type: 'IS_ITEMS_LIST_LOADING'
     })
    yield delay(200);
    yield put({ 
        id: btnDetails.id,
        type: 'SORT_BUTTON_CLICKED_REDUCER'
     })
}

export function* watchButtonClick() {
    yield takeEvery('SORT_BUTTON_CLICKED', buttonsToSortClickAsync)
}
