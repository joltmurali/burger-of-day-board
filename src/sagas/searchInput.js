import { takeEvery, put } from "redux-saga/effects";

export function* searchInputAsync(value) {
    yield put({ 
        type: 'SEARCH_INPUT_ENTERED',
        value: value.value
     })
}

export function* searchInputWatcher() {
    yield takeEvery('SEARCH_INPUT', searchInputAsync);
}